<?php
/* Smarty version 3.1.30, created on 2018-04-17 00:45:52
  from "C:\xampp\htdocs\kalkulator-php\templates\main.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ad527a0563182_66251460',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b1a50b058582d2b146fd0b1d44cbc225a9e13fa1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\kalkulator-php\\templates\\main.html',
      1 => 1523918750,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ad527a0563182_66251460 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<!doctype html>
<html lang="pl">
<head>
<meta charset="utf"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_description']->value)===null||$tmp==='' ? 'Opis domyślny' : $tmp);?>
">
<title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_title']->value)===null||$tmp==='' ? "Tytuł domyślny strony" : $tmp);?>
</title>

<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
<meta name="keywords" content="kalkulator, kredyt, rata" />
<meta http-equiv="X=UA-Compatible" content="IE=edge,chrome=1" />
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7858306295ad527a053c458_47187048', 'addhead');
?>





</head>

<body>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13213002585ad527a055f0a1_82570479', 'header');
?>


<div class="logowanie">
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6761398975ad527a0560e76_50204855', 'content');
?>

</div> <!-- content strony-->

<div class="footer">
    <p>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_253354355ad527a0562469_82082469', 'footer');
?>

    </p>
</div>

</body>
</html><?php }
/* {block 'addhead'} */
class Block_7858306295ad527a053c458_47187048 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 <?php
}
}
/* {/block 'addhead'} */
/* {block 'header'} */
class Block_13213002585ad527a055f0a1_82570479 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="header">
   <!-- <h1><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_title']->value)===null||$tmp==='' ? "Tytuł domyślny strony" : $tmp);?>
</h1>
    <h2><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_header']->value)===null||$tmp==='' ? "Tytuł domyślny nagłówka" : $tmp);?>
</h2>
-->
</div>
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_6761398975ad527a0560e76_50204855 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 Domyślna treść zawartości strony...<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_253354355ad527a0562469_82082469 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 Domyślna treść stopki strony <?php
}
}
/* {/block 'footer'} */
}
