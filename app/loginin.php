
<?php
session_start();

//Kontroler do strony
require_once dirname(__FILE__).'/../config.php';

//załadowanie Smarty
require_once _ROOT_PATH.'/lib/smarty/Smarty.class.php';



//funkcje
function isLogedIn()  //funkcja sprawdza czy uzytkownik jest zalogowany
{
    if(isset($_SESSION['logowanie']))
    {
        if($_SESSION['logowanie']==true)
        {
            
            header('Location: /kalkulator-php/app/kalkulator.php');
            exit();
        }
    }
}

function getParams(&$form) //funkcja pobiera parametry z szablonu
{
$form['login'] = isset($_REQUEST['login']) ? $_REQUEST['login'] : null;
$form['password'] = isset($_REQUEST['password']) ? $_REQUEST['password'] : null;
}



function validate(&$form, &$msgs) //walidacja zmiennych
{
if(!((isset($form['login'])) && isset($form['password']))) //sprawdzenie czy parametry zostały przekazane
{return false;}

if($form['login']=="")
{
    $msgs[]="nie przekazano loginu";
}
if($form['password']=="")
{
    $msgs[]="nie przekazano hasla";
}
if(count($msgs)>0)
 return false;
else 
return true;
}

function LoginIn(&$form,&$msgs)
{
    require_once "connect.php";
    $polaczenie=new mysqli($host,$db_user,$db_password,$db_name);
    if ($polaczenie->connect_errno!=0)
{
    $msgs[] ='".$polaczenie->connect_errno. " Opis:". $polaczenie->connect_error';
    return false;
}
else
{
    $login=$form['login'];
    $haslo=$form['password'];
    $login=htmlentities($login,ENT_QUOTES,"UTF-8");
    $haslo=htmlentities($haslo,ENT_QUOTES,"UTF-8");

    
    
    "SELECT * FROM uzytkownicy WHERE nazwa_uzytkownika='$login' AND haslo='$haslo'";
    
    if($rezultat=@$polaczenie->query(
        sprintf(
        "SELECT * FROM uzytkownicy WHERE nazwa_uzytkownika='%s' AND haslo='%s'",
        mysqli_real_escape_string($polaczenie,$login),
        mysqli_real_escape_string($polaczenie,$haslo)
       )))
    {
    
     $ilu_userow=$rezultat->num_rows;
        if($ilu_userow>0)
        {
        
           $wiersz=$rezultat->fetch_assoc(); 
           
           if($login==$wiersz['nazwa_uzytkownika'] && $haslo==$wiersz['haslo'])
           {
            $_SESSION['id']=$wiersz['id'];
            $rezultat->free();
            return true;
           }
           
           $rezultat->free();
           return true;
        }
        if($ilu_userow==0)
        {
            $msgs[]="Nie znaleziono użytkownika o takiej kombinacji loginu i hasła";

            //$msgs['bladbazy']="Nie znaleziono użytkownika o takiej kombinacji loginu i hasła";
            //echo  $msgs['bladbazy'];
            return false;
        }
       
    }

    else
    {
        echo "Cos poszło nie tak z zapytaniem";
    }


    $polaczenie->close();
}
}



//inicjacja zmiennych
$form=null;
$messages=array();


//wykonanie funkcji


isLogedIn();//sprawdzam czy uzytkownik nie jest juz zalogowany
getParams($form);//pobieram parametry z formularza
if(validate($form,$messages))
{

if(LoginIn($form,$messages)==true)
{
    $_SESSION['logowanie']=true;
    header('Location: /kalkulator-php/app/kalkulator.php');
    exit();
}

}

$smarty = new Smarty();

$smarty->assign('app_url',_APP_URL);
$smarty->assign('root_path',_ROOT_PATH);
$smarty->assign('page_title','Kalkulator kredytowy');
$smarty->assign('page_header','Szablony Smarty');

//pozostałe zmienne niekoniecznie muszą istnieć, dlatego sprawdzamy aby nie otrzymać ostrzeżenia
$smarty->assign('form',$form);
$smarty->assign('messages',$messages);

// 5. Wywołanie szablonu
$smarty->display(_ROOT_PATH.'/app/loginin.html');












?>