<?php


/*
session_start();

//Kontroler do strony
require_once dirname(__FILE__).'/../config.php';

//załadowanie Smarty
require_once _ROOT_PATH.'/lib/smarty/Smarty.class.php';

function isLogedIn()  //funkcja sprawdza czy uzytkownik jest zalogowany
{
    if(isset($_SESSION['logowanie']))
    {
        if($_SESSION['logowanie']==false)
        {
            
            header('Location: kalkulator-php/app/loginin.php');
            exit();
        }
	}
	if(!isset($_SESSION['logowanie']))
    { 
		header('Location: kalkulator-php/app/loginin.php');
		exit();        
    }
}


function getParams(&$form) //funkcja pobiera parametry z szablonu
{
$form['kwota'] = isset($_REQUEST['kwota']) ? $_REQUEST['kwota'] :"" ;
$form['lata'] = isset($_REQUEST['lata']) ? $_REQUEST['lata'] :"" ;
$form['oprocentowanie'] = isset($_REQUEST['oprocentowanie']) ? $_REQUEST['oprocentowanie'] :"" ;
}

function validate(&$form,&$messages)
{	
	if ( ! (isset($form['kwota']) && isset($form['lata'] ) && isset($form['oprocentowanie']))) 
	{
		return false;
	//sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
	}
	if ( $form['kwota'] == "")
	 {
	$messages [] = 'Nie podano kwoty kredytu';
	}
	if ( $form['lata']  == "") {
	$messages [] = 'Nie podano ilosc miesiecy';
	}
	if ( $form['oprocentowanie'] == "") {
	$messages [] = 'Nie podano oprocentowania kredytu';
	}
	
	
	if(! (is_numeric($form['kwota'])))
	{
		$messages[]="Kwota nie jest liczba";
		
	}
	if(! (is_numeric($form['lata'] )))
	{
		$messages[]="Ilość lat nie jest liczba";
		
	}
	if(! (is_numeric($form['oprocentowanie'])))
	{
		$messages[]="Oprocentowanie nie jest liczba";
		
	}
	
	
	if (empty ( $messages )) { // gdy brak błędów
	
	//konwersja parametrów na int
	$form['kwota'] = intval($form['kwota']);
	$form['lata']  = intval($form['lata'] );
	$form['oprocentowanie']=intval($form['oprocentowanie']);
	//wykonanie operacji
	

	
	
	
	
	
	
	//echo "<h2> $form['kwota'] <br> $form['lata']  <br> $form['oprocentowanie'] <br> </h2>";
	
	$form['q']=(1+($form['oprocentowanie']/100)/12);
	$form['iloscrat']=$form['lata'] *12;
	$form['miesiecznaRata']=(($form['kwota']*pow($form['q'],$form['iloscrat']))*(($form['q']-1)/(pow($form['q'],$form['iloscrat'])-1)));  
	
	return true;
	}
	else
	{
	return false;	
	}
}
	
	
//inicjacja zmiennych
$form=null;
$messages=array();



isLogedIn();//sprawdzam czy uzytkownik nie jest juz zalogowany
getParams($form);//pobieram parametry z formularza
validate($form,$messages);



$smarty = new Smarty();

$smarty->assign('app_url',_APP_URL);
$smarty->assign('root_path',_ROOT_PATH);
$smarty->assign('page_title','Kalkulator kredytowy');
$smarty->assign('page_description','Szablonowanie Smarty');
$smarty->assign('page_header','Szablony Smarty');

//pozostałe zmienne niekoniecznie muszą istnieć, dlatego sprawdzamy aby nie otrzymać ostrzeżenia
$smarty->assign('form',$form);
$smarty->assign('messages',$messages);

// 5. Wywołanie szablonu
$smarty->display(_ROOT_PATH.'/app/kalkulator.html');

	*/

require_once dirname(__FILE__).'/../config.php';
require_once $conf->root_path.'/app/KalkCtrl.class.php';
$ctrl=new KalkCtrl();
$ctrl->process();

?>
