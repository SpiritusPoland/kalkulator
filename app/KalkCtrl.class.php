<?php

require_once $conf->root_path.'/lib/smarty/Smarty.class.php';
require_once $conf->root_path.'/lib/Messages.class.php';
require_once $conf->root_path.'/app/KalkForm.class.php';



class KalkCtrl{

    private $msgs;
    private $form;


    public function __construct()
    {
        $this->msgs=new Messages();
        $this->form=new KalkForm();
    }


    public function getParams()
    {
        $this->form->kwota = isset($_REQUEST['kwota']) ? $_REQUEST['kwota'] :"" ;
        $this->form->lata = isset($_REQUEST['lata']) ? $_REQUEST['lata'] :"" ;
        $this->form->oprocentowanie = isset($_REQUEST['oprocentowanie']) ? $_REQUEST['oprocentowanie'] :"" ;
    }

    public function validate()
    {
        if ( ! (isset($this->form->kwota ) && isset($this->form->lata ) && isset($this->form->oprocentowanie)))
        {
            return false;
            //sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
        }
        if ( $this->form->kwota == "")
        {
            $this->msgs->addError('Nie podano kwoty kredytu');
        }
        if ( $this->form->lata  == "") {
            $this->msgs->addError ('Nie podano ilosc miesiecy');
        }
        if ( $this->form->oprocentowanie == "") {
            $this->msgs->addError ('Nie podano oprocentowania kredytu');
        }


        if(! (is_numeric( $this->form->kwota)))
        {
            $this->msgs->addError("Kwota nie jest liczba");

        }
        if(! (is_numeric($this->form->lata )))
        {
            $this->msgs->addError("Ilość lat nie jest liczba");

        }
        if(! (is_numeric($this->form->oprocentowanie)))
        {
            $this->msgs->addError("Oprocentowanie nie jest liczba");

        }


        if ($this->msgs->isEmpty())
        { // gdy brak błędów

            //konwersja parametrów na int
            $this->form->kwota = intval($this->form->kwota);
            $this->form->lata = intval($this->form->lata);
            $this->form->oprocentowanie=intval($this->form->oprocentowanie);
            //wykonanie operacji


            $this->form->q=(1+($this->form->oprocentowanie/100)/12);
            $this->form->iloscrat=$this->form->lata*12;
            $this->form->miesiecznaRata=(($this->form->kwota*pow($this->form->q,$this->form->iloscrat))*(($this->form->q-1)/(pow($this->form->q,$this->form->iloscrat)-1)));

            return true;
        }
        else
        {
            return false;
        }
    }
    public function generateView()
    {

        global $conf;
        $smarty =new Smarty();
        $smarty->assign('app_url',$conf->app_url);
        $smarty->assign('root_path',$conf->root_path);
        $smarty->assign('page_title','Kalkulator kredytowy');
        $smarty->assign('page_description','Szablonowanie Smarty');
        $smarty->assign('page_header','Szablony Smarty');   //pozostałe zmienne niekoniecznie muszą istnieć, dlatego sprawdzamy aby nie otrzymać ostrzeżenia
        $smarty->assign('form',$this->form);




        $smarty->assign('msgs',$this->msgs->getErrors());
        //5. Wywołanie szablonu
        $smarty->display($conf->root_path.'/app/kalkulator.html');
    }

    public function process()
    {
        $this->getParams();
        $this->validate();
        $this->generateView();

    }

}