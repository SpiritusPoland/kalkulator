<?php
/* Smarty version 3.1.30, created on 2018-04-17 04:47:20
  from "C:\xampp\htdocs\kalkulator-php\app\loginin.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ad56038cdb217_41362003',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b217a6ac1782a546f1b42487e3daa7401383c9b8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\kalkulator-php\\app\\loginin.html',
      1 => 1523933226,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/main.html' => 1,
  ),
),false)) {
function content_5ad56038cdb217_41362003 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11795400705ad56038cbb770_81509485', 'addhead');
?>



	<!-- Fixed navbar -->

    <!-- /.navbar -->
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7839427215ad56038cbdc43_91387292', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_808481195ad56038cd66b8_60589792', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21143529055ad56038cda8e0_53071414', 'footer');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:../templates/main.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'addhead'} */
class Block_11795400705ad56038cbb770_81509485 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- KOMETNARZ POMOCNICZY!!! -->
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/images/gt_favicon.png">
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/font-awesome.min.css">
	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/html5shiv.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

<?php
}
}
/* {/block 'addhead'} */
/* {block 'header'} */
class Block_7839427215ad56038cbdc43_91387292 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<header id="head" class="secondary">Logowanie</header>
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_808481195ad56038cd66b8_60589792 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<!-- container -->
	<div class="container">
		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">Zaloguj się</h1>
				</header>
				</article>
				<article class="col-xs-12 maincontent">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">Zaloguj się do swojego konta</h3>

							<hr>
							
							<form action="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/app/loginin.php" method="post">
								<div class="top-margin">
									<label>Login: <span class="text-danger">*</span></label>
									<input id="login" name="login" type="text" class="form-control" placeholder="tu wpisz login" value="<?php echo $_smarty_tpl->tpl_vars['form']->value['login'];?>
"/>
								</div>
								<div class="top-margin">
									<label>Hasło: <span class="text-danger">*</span></label>
									<input id="password" name="password" type="password" placeholder="Tu wpisz hasło" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['form']->value['password'];?>
">
								</div>

								<hr>

								<div class="row">
									<div class="col-lg-8">
										<b><a href="">Zapomniane hasło?</a></b>
									</div>
									<div class="col-lg-4 text-right">
									<button class="btn btn-action" type="submit">Zaloguj</button>
									</div>
								</div>
							</form>
							<?php if (isset($_smarty_tpl->tpl_vars['messages']->value)) {?>
							<?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
							<h4>Wystąpił problem z logowaniem : <h4/>
								<ol class="err">
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['messages']->value, 'msg');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
?>
									<li><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</li>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

								</ol>
								<?php }?>
								<?php }?>
						</div>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_21143529055ad56038cda8e0_53071414 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<footer id="footer" class="top-space">
		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; 2014, Łukasz Malinowski. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a>
							</p>
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>
<?php echo '<script'; ?>
 src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/template.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/headroom.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/jQuery.headroom.min.js"><?php echo '</script'; ?>
>
<!-- JavaScript libs are placed at the end of the document so the pages load faster -->


<?php
}
}
/* {/block 'footer'} */
}
