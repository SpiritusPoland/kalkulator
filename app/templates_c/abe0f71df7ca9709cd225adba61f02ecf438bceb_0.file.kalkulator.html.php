<?php
/* Smarty version 3.1.30, created on 2018-04-17 06:19:49
  from "C:\xampp\htdocs\kalkulator-php\app\kalkulator.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ad575e5bc8818_01331441',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'abe0f71df7ca9709cd225adba61f02ecf438bceb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\kalkulator-php\\app\\kalkulator.html',
      1 => 1523938786,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/main.html' => 1,
  ),
),false)) {
function content_5ad575e5bc8818_01331441 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19062763105ad575e5baf6c8_41747586', 'addhead');
?>



        <!-- Fixed navbar -->

        <!-- /.navbar -->
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21246255765ad575e5bb0ca0_85864591', 'header');
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1382279575ad575e5bc5715_42591300', 'content');
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18557988345ad575e5bc7e82_99247471', 'footer');
?>
</title>
</head>
<body>

</body>
</html><?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:../templates/main.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'addhead'} */
class Block_19062763105ad575e5baf6c8_41747586 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <!-- KOMETNARZ POMOCNICZY!!! -->
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/images/gt_favicon.png">
        <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/font-awesome.min.css">
        <!-- Custom styles for our template -->
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/bootstrap-theme.css" media="screen" >
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/css/main.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/respond.min.js"><?php echo '</script'; ?>
>
        <![endif]-->

        <?php
}
}
/* {/block 'addhead'} */
/* {block 'header'} */
class Block_21246255765ad575e5bb0ca0_85864591 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header id="head" class="secondary">Logowanie</header>
        <?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_1382279575ad575e5bc5715_42591300 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <!-- container -->
        <div class="container">
            <div class="row">

                <!-- Article main content -->
                <article class="col-xs-12 maincontent">
                    <header class="page-header">
                        <h1 class="page-title">Oblicz:</h1>
                    </header>
                </article>
                <article class="col-xs-12 maincontent">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 class="thin text-center">Oblicz swoją ratę kredytu:</h3>

                                <hr>

                                <form action="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/app/kalkulator.php" method="post">
                                    <div class="top-margin">
                                        <label>Kwota kredytu: <span class="text-danger">*</span></label>
                                        <input id="kwota" name="kwota" type="text" placeholder="kwota kredytu" class="form-control"  value="<?php echo $_smarty_tpl->tpl_vars['form']->value->kwota;?>
"/>
                                    </div>
                                    <div class="top-margin">
                                        <label>Ilość lat: <span class="text-danger">*</span></label>
                                        <input id="lata" name="lata" type="text" placeholder="Ilość lat" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->lata;?>
">
                                    </div>
                                    <div class="top-margin">
                                        <label>Oprocentowanie: <span class="text-danger">*</span></label>
                                        <input id="oprocentowanie" name="oprocentowanie" type="text" class="form-control" placeholder="Oprocentowanie np dla 2% wpisz 2" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->oprocentowanie;?>
"/>
                                    </div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-lg-4 text-left">
                                            <button class="btn btn-action" type="submit">Oblicz</button>
                                        </div>
                                    </form>
                                <div class="col-lg-4 text-right">
                                    <form action="../app/logout.php" method="get" >
                                        <button class="btn btn-action" type="submit">Wyloguj</button>
                                    </form>
                                </div>

                                        <?php if (isset($_smarty_tpl->tpl_vars['form']->value->miesiecznaRata)) {?>
                                        <br/>
                                        <div style="margin: 20px; padding: 10px; border-radius: 5px;
	background-color: #ff0; width:1fr; color:black">
                                            Miesieczna Rata: <?php echo $_smarty_tpl->tpl_vars['form']->value->miesiecznaRata;?>

                                            <?php }?>
                                    </div>


                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['msgs']->value)) {?>
                            <?php if (count($_smarty_tpl->tpl_vars['msgs']->value)) {?>
                            <h4>Wystąpił problem z obliczaniem: <h4/>
                                <ol class="err">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['msgs']->value, 'msg');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->value) {
?>
                                    <li><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</li>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </ol>
                                <?php }?>
                                <?php }?>
                        </div>

                    </div>

                </article>
                <!-- /Article -->

            </div>
        </div>	<!-- /container -->
        <?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_18557988345ad575e5bc7e82_99247471 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <footer id="footer" class="top-space">
            <div class="footer2">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6 widget">
                            <div class="widget-body">
                                <p class="text-right">
                                    Copyright &copy; 2014, Łukasz Malinowski. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a>
                                </p>
                            </div>
                        </div>
                    </div> <!-- /row of widgets -->
                </div>
            </div>
        </footer>
        <?php echo '<script'; ?>
 src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/template.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/headroom.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['app_url']->value;?>
/assets/js/jQuery.headroom.min.js"><?php echo '</script'; ?>
>
        <!-- JavaScript libs are placed at the end of the document so the pages load faster -->


        <?php
}
}
/* {/block 'footer'} */
}
