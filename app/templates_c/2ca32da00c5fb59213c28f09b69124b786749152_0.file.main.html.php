<?php
/* Smarty version 3.1.30, created on 2018-04-17 03:29:34
  from "C:\xampp\htdocs\kalkulator-php\templates\main.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ad54dfe0aba43_47532937',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2ca32da00c5fb59213c28f09b69124b786749152' => 
    array (
      0 => 'C:\\xampp\\htdocs\\kalkulator-php\\templates\\main.html',
      1 => 1523918750,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ad54dfe0aba43_47532937 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<!doctype html>
<html lang="pl">
<head>
<meta charset="utf"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_description']->value)===null||$tmp==='' ? 'Opis domyślny' : $tmp);?>
">
<title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_title']->value)===null||$tmp==='' ? "Tytuł domyślny strony" : $tmp);?>
</title>

<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
<meta name="keywords" content="kalkulator, kredyt, rata" />
<meta http-equiv="X=UA-Compatible" content="IE=edge,chrome=1" />
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3257092955ad54dfe0a2b23_63841336', 'addhead');
?>





</head>

<body>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7949599175ad54dfe0a74b2_42248828', 'header');
?>


<div class="logowanie">
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15962943505ad54dfe0a9d72_88892258', 'content');
?>

</div> <!-- content strony-->

<div class="footer">
    <p>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16618255345ad54dfe0ab1e6_19043847', 'footer');
?>

    </p>
</div>

</body>
</html><?php }
/* {block 'addhead'} */
class Block_3257092955ad54dfe0a2b23_63841336 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 <?php
}
}
/* {/block 'addhead'} */
/* {block 'header'} */
class Block_7949599175ad54dfe0a74b2_42248828 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="header">
   <!-- <h1><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_title']->value)===null||$tmp==='' ? "Tytuł domyślny strony" : $tmp);?>
</h1>
    <h2><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page_header']->value)===null||$tmp==='' ? "Tytuł domyślny nagłówka" : $tmp);?>
</h2>
-->
</div>
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_15962943505ad54dfe0a9d72_88892258 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 Domyślna treść zawartości strony...<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_16618255345ad54dfe0ab1e6_19043847 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 Domyślna treść stopki strony <?php
}
}
/* {/block 'footer'} */
}
